# API  #
La ruta con los libros en producción es:
http://backeros.com/api/books

En tu local

localhost/api/books


la ruta utiliza los metodos.
GET, POST, UPDATE y DELETE para leer, crear, editar y borrar respectivamente.

Ejemplo:

GET     (get all) localhost/api/books/
GET     (get one) localhost/api/books/{id}
POST    (create) localhost/api/books/
``` javascript
let request = {
    {
      "name": "Hola holasdasdasdasda",
      "author": "George Orwell",
      "categories_id": "5",
      "published_at": "1949-06-08",
      "users_id": null //can be a valid user_id
    }
}
```
PUT     (update) localhost/api/books/{id}
``` javascript
let request = {
    {
      "name": "Hola holasdasdasdasda",
      "author": "George Orwell",
      "categories_id": "5",
      "published_at": "1949-06-08",
      "users_id": null //can be a valid user_id
    }
}
```
DELETE  (update) localhost/api/books/{id}





Para instalar la api solo necesitas poner esta carpeta en al raíz de tu server

ejemplo:   ->  /var/www/html/api

En caso de que instales en una subcarpeta de raíz cambia la siguiente linea
en el archivo /Core/configs.inc

```php
static $urlBase = '/my_awesome_folder/';
```
En la carpeta /Dump hay un sql con la base de datos

para configurar la base de datos ve al archivo /Core/configs.inc

Y cambia las siguientes lineas.

```php
abstract class ModelConfig extends Commons
{
    public $defaultConnection = 'local';
    public $connectionCfg = [
        'local' => [
            'host' => 'my_awesome_host',
            'user' => 'john_doe',
            'name' => 'data_base',
            'password' => 'my_super_secret_password'
        ]
    ];
}
```
El archivo .htaccess es importante, si por alguna razón no funciona habilita tu modulo de sobrescritura en apache
Y reinicia tu server