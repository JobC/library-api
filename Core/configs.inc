<?php

abstract class AppConfig extends Commons
{
    static $coreVersion = '3.5.5.1';
    static $urlBase = '/';
    static $appName = 'Api';
    static $appVersion = '0.0.1';
    static $loadResourcesFrom = 'media';
    static $live_site = '/';
    static $debug = true;
    static $debug_disablecss = false;
    static $debug_disablejs = false;
    static $google_maps_api_key = "AIzaSyASS9b8PvWs8LrWBAYkqiDG5DCH7Ba_2HA";

    static $smtpMailing = false;
    static $smtpConfig = [
        'port' => 25,
        'host' => 'host',
        'username' => 'user@example.com',
        'password' => '',
        'charset' => 'UTF-8',
        'remitenteEmail' => 'no-reply@example.com',
        'remitente' => '¡'
    ];

    static $defaults = [
        'controller' => 'api',
        'action' => 'index',
        'template' => 'api',
    ];

    static $languagesCFG = [
        'allow_translations' => false,
        'default_prefix' => 'es',
        'debug_translation' => false,
        'dump_untranslated' => false,
        'dictionaries' => [
            [
                'name' => 'Español (México)',
                'file' => 'es_mx',
                'prefix' => 'es'
            ],
            [
                'name' => 'English (U. S. A.)',
                'file' => 'en_us',
                'prefix' => 'en'
            ],
        ]
    ];
    static $allow_pretty_url = true;
    static $time_limit = 5;
    static $memory_limit = '150M';
    static $timezone = "America/Mexico_City";
}

abstract class ModelConfig extends Commons
{
    public $defaultConnection = 'local';
    public $connectionCfg = [
        'local' => [
            'host' => 'localhost',
            'user' => 'col05',
            'name' => 'library',
            'password' => 'Job1'
        ]
    ];
}
