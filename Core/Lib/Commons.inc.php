<?php

/**
 * Commons Abstract Class
 *
 * Establece las funciones de comúnes dentro de todas
 * las clases primeras al ser heredadas por los constructores
 * de configuración.
 *
 * @Version 1.0
 */
abstract class Commons
{

    public static $context = null;

    public function is_post()
    {
        return (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST');
    }

    public function is_put()
    {
        return (strtoupper($_SERVER['REQUEST_METHOD']) == 'PUT');
    }

    public function is_delete()
    {
        return (strtoupper($_SERVER['REQUEST_METHOD']) == 'DELETE');
    }

    public function is_get()
    {
        return (strtoupper($_SERVER['REQUEST_METHOD']) == 'GET');
    }

    public function is_ajax()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }

    public function get_request()
    {
        if (!isset($this->controller)) {
            die('Acceso incorrecto');
        }

        $request = array(
            'context' => self::$context,
            'controller' => strtolower($this->controller),
            'action' => strtolower($this->action)
        );

        if (!is_null($request['context'])) {
            $request['action'] = preg_replace("/^(" . $request['context'] . "\_)/", null, $request['action']);
        }

        if (!empty($this->get)) {
            $request = array_merge($request, $this->get);
        }

        return ($request);
    }

    /* 200 Ok */
    public function set_200($data = null)
    {
        header("HTTP/1.0 200 Ok");
        if ($data) {
            $data['status_code'] = 200;
            if (!isset($data['description'])) {
                $data['description'] = 'Sucess :)';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Ok';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 200,
                'description' => 'Sucess :)',
                'message' => 'Ok'
            ));
        }
    }

    public function set_201($data = null)
    {
        header("HTTP/1.0 201 Created");
        if ($data) {
            $data['status_code'] = 201;
            if (!isset($data['description'])) {
                $data['description'] = 'Sucess :)';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Created';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 201,
                'description' => 'Sucess :)',
                'message' => 'Created'
            ));
        }
    }

    /* 204 No Content */
    public function set_204()
    {
        header("HTTP/1.0 204 No Content");
        $this->json(array(
            'status_code' => 204,
            'description' => 'Something bad happened :(',
            'message' => 'No Content'
        ));
    }

    /* 400 Bad Request */
    public function set_400($data = null)
    {
        header("HTTP/1.0 400 Bad Request");
        if ($data) {
            $data['status_code'] = 400;
            if (!isset($data['description'])) {
                $data['description'] = 'Something bad happened :(';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Bad Request';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 400,
                'description' => 'Something bad happened :(',
                'message' => 'Bad Request'
            ));
        }
    }

    /* 403 Forbidden */
    public function set_403($data = null)
    {
        header("HTTP/1.0 403 Forbidden");
        if ($data) {
            $data['status_code'] = 403;
            if (!isset($data['description'])) {
                $data['description'] = 'Something bad happened :(';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Forbidden';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 403,
                'description' => 'Something bad happened :(',
                'message' => 'Forbidden'
            ));
        }
    }

    /* 404 Not Found */
    public function set_404($data = null)
    {
        header("HTTP/1.0 404 Not Found");
        if ($data) {
            $data['status_code'] = 404;
            if (!isset($data['description'])) {
                $data['description'] = 'Something bad happened :(';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Not Found';
            }
            $this->json($data);
        } else {
            $this->json([
                'status_code' => 404,
                'description' => 'Something bad happened :(',
                'message' => 'Not Found'
            ]);
        }
    }

    /* 405 Method Not Allowed */
    public function set_405()
    {
        header("HTTP/1.0 405 Method Not Allowed");
        $this->json(
            array(
                'status_code' => 405,
                'description' => 'Something bad happened :(',
                'message' => 'Method Not Allowed'
            )
        );
    }

    /* 409 Conflict */
    public function set_409($data = null)
    {
        header("HTTP/1.0 409 Conflict");
        if ($data) {
            $data['status_code'] = 409;
            if (!isset($data['description'])) {
                $data['description'] = 'Something bad happened :(';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Conflict';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 409,
                'description' => 'Something bad happened :(',
                'message' => 'Conflict'
            ));
        }
    }

    /* 422 Unprocessable Entity */
    public function set_422($data = null)
    {
        header("HTTP/1.0 422 Unprocessable Entity");
        if ($data) {
            $data['status_code'] = 422;
            if (!isset($data['description'])) {
                $data['description'] = 'Something bad happened :(';
            }
            if (!isset($data['message'])) {
                $data['message'] = 'Unprocessable Entity';
            }
            $this->json($data);
        } else {
            $this->json(array(
                'status_code' => 422,
                'description' => 'Something bad happened :(',
                'message' => 'Unprocessable Entity'
            ));
        }
    }

    /**
     * Load
     *
     *  Crea un objeto y clase definidas.
     *
     * El primer parámetro es obligatorio y debe ser el nombre del archivo Inc, y
     * coincidir
     * con el nombre de la clase, por ejemplo "Session.inc.php" y la clase "Session".
     *
     * El segundo parámetro es opcional y corresponde a la ubicación, por defecto
     * será raíz, done
     * se estima que deban ir todos los plugins que puedan ser accedidos por
     * controlador, modelos y vistas, como
     * el caso de sesion.
     *
     * @version 1.0
     * */

    public function load($class_name, $class_loc = null, $subs = null, $prefix = '.inc')
    {

        $object_name = ucfirst(strtolower($class_name));

        $filename = APP::path($class_loc, $subs) . $class_name . $prefix . ".php";

        if (file_exists($filename)) {

            include_once($filename);

            if ($prefix != '.inc') {
                $class_name .= ucfirst(strtolower($prefix));
            }

            if (class_exists($class_name)) {
                $this->{$object_name} = new $class_name;
            } else {
                die(sprintf('Library Found but Class names is missmatch! [%s]', $class_name));
            }

        } else {

            die(sprintf('Library Not Found! [%s]', $filename));

        }
    }

    /**
     * Path
     *
     * Establece la ubicación física de un archivio a partir de la ubicación
     * del archivo Core.
     *
     * @Version 1.2
     */
    static public function path($lib = null, $subs = array())
    {
        if (!is_null($lib)) {
            $lib .= DIRECTORY_SEPARATOR;
        }

        if (is_array($subs)) {
            foreach ($subs as $subfolder) {
                $lib .= $subfolder . DIRECTORY_SEPARATOR;
            }
        }

        return (realpath(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . $lib;
    }

    static public function temp($lib = null, $subs = array())
    {

        if (is_array($subs)) {
            foreach ($subs as $subfolder) {
                $lib .= $subfolder . DIRECTORY_SEPARATOR;
            }
        }

        $return = (realpath(dirname(dirname(dirname(__FILE__))))) . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $lib;

        return $return;
    }

    /**
     * Go To
     *
     * Redirecciona al usuario a una URL distinta, limpiando la posibilidad
     * de ejecución de código aposteriori.
     *
     * @Version 1.0
     */

    public function go_to($url)
    {
        if (empty($url)) {
            $this->set_404();
        }
        header('location: ' . $this->url($url));
        die();
    }

    /**
     * Referer
     *
     * Obtiene el Referente de la página actual
     *
     * @Version 1.0
     */

    public function referer()
    {

        if (!empty($_SERVER['HTTP_REFERER'])) {
            return $_SERVER['HTTP_REFERER'];
        }

        return false;
    }

    /**
     * Referrer
     *
     * Obtiene el Referente de la página actual ()
     *
     * @Version 1.0
     */

    public function referrer()
    {

        return $this->referer();

    }

    /**
     * URL
     *
     * Funcion que permite la creación de URL siguiendo el modo
     * [URLBase]/controlador/accion/parametros
     *
     * El primer parámetro puede ser un string o un array que indique
     * el objeto que se desea alcanzar en la URL. El segundo parámetro
     * es un booleano que determina si la liga se incluirá con todo
     * y el nombre del servidor
     *
     * BUG Corregido | Ahora elimina las diagonales dobles.
     *
     * UPDATE | Ahora permite generar ligas en funcion del lenguaje seleccionado cuando exista dicha configuración.
     * UPDATE | Ahora, al pasarle el parámetro $this -> request lo valida con la APP para generar un request completo.
     * UPDATE | 1.5 Ahora, al pasarle bajo el formato de Array una URL una ruta
     * UPDATE | 1.5 Ahora, al pasarle bajo el formato de Array una URL una ruta
     *
     * El parametro $treat_as_media en  verdadero considera que la URL es un medio en el servidor, por lo que no se Agrega
     * el parámetro index.php?r=. (Este parámetro sólo es funcional para $urls en formato STRING).
     *
     * @Version 1.6
     * @Date   11/08/2015
     */

    public function url($sRout = array(), $printServerName = false, $addRefresher = false, $treat_as_media = false)
    {

        // INIT
        $prettyURL = (isset(APP::$allow_pretty_url)) ? APP::$allow_pretty_url : true;

        $serverName = null;

        if ($printServerName) {
            $serverName = '//' . ($_SERVER['HTTP_HOST']);
        }

        $dbStr = null;

        if (($addRefresher and $prettyURL) and is_array($sRout)) {
            $dbStr = '?_=' . uniqid();
        } else {
            if (!$prettyURL and $addRefresher) {
                $dbStr = '&_=' . uniqid();
            }
        }

        // BOOTSTRAP PARA REQUEST
        if (is_array($sRout) and $sRout == APP::$request_proccessed) {
            // ASIGNACION
            $sRout = APP::$request_full_array;

            // FORMATO DE PARAMS
            if (isset($sRout['getAttrs']) and !empty($sRout['getAttrs'])) {
                foreach ($sRout['getAttrs'] as $p) {
                    $sRout[] = $p;
                }
                unset($sRout['getAttrs']);
            }
        }

        // PROCESA ARREGLO DE URL EXPLÍCITO (MEJOR)
        if (is_array($sRout)) {

            $str = "";

            if (isset($sRout['context'])) {
                if (is_string($sRout['context'])) {
                    $str .= strtolower(trim($sRout['context'])) . '/';
                }
                unset($sRout['context']);
            } else {
                if (!is_null(Commons::$context)) {
                    $str .= strtolower(trim(Commons::$context)) . '/';
                }
            }

            if (isset(AppConfig::$languagesCFG)
                and AppConfig::$languagesCFG['allow_translations']
            ) {
                if (isset($sRout['lang'])) {
                    if (is_string($sRout['lang'])) {
                        $str .= strtolower(trim($sRout['lang'])) . '/';
                    }
                    unset($sRout['lang']);
                } else {
                    if (!is_null(Translate::$selectedLang['prefix'])) {
                        $str .= strtolower(Translate::$selectedLang['prefix']) . '/';
                    }
                }
            }

            if (isset($sRout['controller'])) {
                $str .= strtolower(trim($sRout['controller'])) . '/';
                unset($sRout['controller']);
            }

            if (isset($sRout['action'])) {
                $str .= strtolower(trim($sRout['action'])) . '/';
                unset($sRout['action']);
            }

            foreach ($sRout as $param) {
                if (!is_array($param)) {
                    $str .= $this->url_style($param) . '/';
                } else {
                    foreach ($param as $k => $p) {
                        $param[$k] = $this->url_style($p);
                    }
                }
            }

            $str = preg_replace('/[ ]|\/$/', '', $str);

            $str = preg_replace('/\/\//', '/', $str);

            // NON PRETTY URL FIX
            if (!$prettyURL) {
                $str = preg_replace('/^\//', null, $str);
                $str = "index.php?r=" . $str;
            }

            // breakpoint($serverName . AppConfig::$urlBase . $str . $dbStr."array");
            return $serverName . AppConfig::$urlBase . $str . $dbStr;

        } elseif (is_string($sRout)) {

            if (!preg_match('/^http[s]{0,1}\:/', $sRout)) {
                $sUrl = AppConfig::$urlBase;
                if (!$prettyURL and !$treat_as_media) {
                    $sUrl .= 'index.php?r=';
                }
            } else {
                $sUrl = '';
            }

            $final_str = $sUrl . $sRout;

            // Remueve diagonales finales
            $final_str = preg_replace('/\/$/', null, $final_str);

            // Remueve diagonales dobles
            $final_str = preg_replace('/\/+/', '/', $final_str);

            // Pero si se han removido las dobles diagonales http:// o https:// las
            // reinserta.
            $final_str = preg_replace('/\:\//', '://', $final_str);
            $response = $serverName . $final_str . $dbStr;

            return ($response);
        }

    }

    /**
     * url_style
     *
     * Devuelve un string estilo url según el concepto del MVC Lite
     *
     * @Version 1.5
     * @Date   17/11/2015
     */

    public function url_style($str)
    {
        $str = urlencode($this->supress_non_alphanumeric(trim($str)));
        $str = strtolower(trim($str));
        return $str;
    }

    /**
     * supress_non_alphanumeric
     *
     * Devuelve un string sin los caracteres no latinos. Útil para URLS y tokens.
     *
     * @Version 1.5
     * @Date   17/11/2015
     */

    public function supress_non_alphanumeric($str)
    {
        $from = array(
            'á',
            'Á',
            'é',
            'É',
            'í',
            'Í',
            'ó',
            'Ó',
            'ú',
            'Ú',
            'ü',
            'Ü',
            'ñ',
            'Ñ',
            '¿',
            '?',
            '¡',
            '!',
            '#',
            '*',
            '<',
            '>',
            '(',
            ')',
            '[',
            ']',
        );
        $to = array(
            'a',
            'A',
            'e',
            'E',
            'i',
            'I',
            'o',
            'O',
            'u',
            'U',
            'u',
            'U',
            'n',
            'N',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
        );
        return str_replace($from, $to, $str);
    }

    /**
     * hashify
     *
     * Convierte cualquier cadena a Hash, con el SecuritySalt definido, si es que existe, de otra forma, toma el path normal.
     *
     * @Version 1.0
     */

    public function hashify($string)
    {
        if (isset(AppConfig::$securitySalt)) {
            $securitySalt = AppConfig::$securitySalt;
        } else {
            $securitySalt = $this->path();
        }

        if (!is_string($string)) {
            die('Solo se puede pasar a hashify un string');
        }

        return md5($string . $securitySalt);
    }

    // GET LANG
    public function get_lang()
    {
        if (class_exists('Translate')) {
            return (Translate::$selectedLang['prefix']);
        } else {
            return false;
        }
    }

    // GET LANG
    public function get_lang_file()
    {
        if (class_exists('Translate')) {
            return (Translate::$selectedLang['file']);
        } else {
            return false;
        }
    }

    // IS TRANSLATABLE
    public function is_translatable()
    {
        return class_exists('Translate');
    }

    public function cleanString($String)
    {
        $String = str_replace(array('á', 'à', 'â', 'ã', 'ª', 'ä'), "a", $String);
        $String = str_replace(array('Á', 'À', 'Â', 'Ã', 'Ä'), "A", $String);
        $String = str_replace(array('Í', 'Ì', 'Î', 'Ï'), "I", $String);
        $String = str_replace(array('í', 'ì', 'î', 'ï'), "i", $String);
        $String = str_replace(array('é', 'è', 'ê', 'ë'), "e", $String);
        $String = str_replace(array('É', 'È', 'Ê', 'Ë'), "E", $String);
        $String = str_replace(array('ó', 'ò', 'ô', 'õ', 'ö', 'º'), "o", $String);
        $String = str_replace(array('Ó', 'Ò', 'Ô', 'Õ', 'Ö'), "O", $String);
        $String = str_replace(array('ú', 'ù', 'û', 'ü'), "u", $String);
        $String = str_replace(array('Ú', 'Ù', 'Û', 'Ü'), "U", $String);
        $String = str_replace(array('[', '^', '´', '`', '¨', '~', ']'), "", $String);
        $String = str_replace("ç", "c", $String);
        $String = str_replace("Ç", "C", $String);
        $String = str_replace("ñ", "n", $String);
        $String = str_replace("Ñ", "N", $String);
        $String = str_replace("Ý", "Y", $String);
        $String = str_replace("ý", "y", $String);

        $String = str_replace("&aacute;", "a", $String);
        $String = str_replace("&Aacute;", "A", $String);
        $String = str_replace("&eacute;", "e", $String);
        $String = str_replace("&Eacute;", "E", $String);
        $String = str_replace("&iacute;", "i", $String);
        $String = str_replace("&Iacute;", "I", $String);
        $String = str_replace("&oacute;", "o", $String);
        $String = str_replace("&Oacute;", "O", $String);
        $String = str_replace("&uacute;", "u", $String);
        $String = str_replace("&Uacute;", "U", $String);
        return $String;
    }

    public function cleanTitle($string = '')
    {
        $title = (string)$string;
        $title = mb_strtolower($string);
        $title = strip_tags($title);
        $title = trim($title, ' ');
        $title = ucfirst($title);
        return $title;
    }

    public function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                if (isset($_SERVER['HTTP_X_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                } else {
                    if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                    } else {
                        if (isset($_SERVER['HTTP_FORWARDED'])) {
                            $ipaddress = $_SERVER['HTTP_FORWARDED'];
                        } else {
                            if (isset($_SERVER['REMOTE_ADDR'])) {
                                $ipaddress = $_SERVER['REMOTE_ADDR'];
                            } else {
                                $ipaddress = null;
                            }
                        }
                    }
                }
            }
        }
        return $ipaddress;
    }
}
