<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="es"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $this -> title  . $this -> name?></title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="author" content="">
        <meta name="copyright" content="">
        <meta name="creator" content="">
        <meta name="distribution" content="global">
        <meta name="rating" content="general">
        <meta name="robots" content="all">
        <meta name="revisit-after" content="14 days">


        <link rel="shortcut icon" href="<?php echo $this -> root?>img/touch-icons/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo $this -> root?>img/touch_icons/apple-touch-icon.png">

        <!-- Dublin Core -->
        <meta name="DC.title" content="">
        <meta name="DC.subject" content="">
        <meta name="DC.creator" content="">

        <!-- CSS -->
        <?php echo $this -> Html -> css('jquery-ui')?>

        <!-- Modernizr -->
        <?php // echo $this -> Html -> script('modernizr-2.6.2.min')?>
        <?php echo $this -> Html -> script('jquery');?>
        <?php echo $this -> Html -> script('jquery-ui.min');?>
    
		<link href="<?php echo $this -> url('favicon.ico')?>" rel="icon" type="image/x-icon" />

    </head>
    <body class="<?php echo $this -> controller . " " . $this -> view ?>">

        <!--[if lt IE 7]>
            <p class="chromeframe">Tu navegador es <strong>Obsoleto</strong>. Por favor <a href="http://browsehappy.com/">actualizalo</a> o <a href="https://www.google.com/chrome/browser/">instala Google Chrome</a> para mejorar tu experiencia.</p>
        <![endif]-->

        <!--[if lt IE 8]>
            <p class="chromeframe">Tu navegador es <strong>Obsoleto</strong>. Por favor <a href="http://browsehappy.com/">actualizalo</a> o <a href="https://www.google.com/chrome/browser/">instala Google Chrome</a> para mejorar tu experiencia.</p>
        <![endif]-->

        
         
        <section id="mainHolder">
        	<?php
			if ( isset( $banners ) ) {
				$this -> element('slides', array('banners' => $banners));
			}
			?>
			
			<?php echo $this -> contents?>
            <?php $this -> element('flash');?>
			
        </section>  
      
        <!-- SELECTIVIZR -->
        <!--[if (gte IE 6)&(lte IE 8)]>
        	<?php echo $this -> Html -> script('polyfills/selectivizr-min')?>
        <![endif]-->

        <?php $this -> element('analytics')?>

    </body>
</html>
