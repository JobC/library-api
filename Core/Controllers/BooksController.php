<?php

class Books extends AppController
{
    public $models = ['Books'];
    public function index()
    {
        $id = (count($this->get) > 0) ? $this->get[0] : null;
        $id = (is_numeric($id)) ? $id : null;

        if ($this->is_get()) {
            /**
             * In da get method we call one or many items
             **/
            $this->Books->search($id);

        } elseif ($this->is_post()) {
            /**
             * Da post method is 4 create a new one item
             **/
            $this->Books->store($this->request);

        } elseif ($this->is_put()) {
            /**
             * update one item
             **/
            if(is_null($id)){
                $this->set_400(['description' => 'id is required']);
            }
            $this->Books->store($this->request, $id);

        } elseif ($this->is_delete()) {
            /**
             * delete one item
             **/
            $this->Books->destroy($id);

        }else{
            $this->set_405();
        }
    }
}
