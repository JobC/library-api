<?php

class AppController extends Controller
{

    public $components = array();
    public $helpers = array('Form', 'Js');
    public $request = null;

    public function beforeAction()
    {
        header('Access-Control-Allow-Origin: *');
        $this->request = $this->build_data_from_pure_json();
    }

    public function beforeTemplate()
    {
        $this->_check_template();
    }

    private function _check_template()
    {
        if (Routes::is_ajax()) {
            $this->template = 'ajax';
        }
    }

    public function build_data_from_pure_json()
    {
        $request = file_get_contents("php://input");
        $request = json_decode($request, true);
        if (empty($request) OR !is_array($request)) {
            $request = [];
        }
        return $request;
    }


}
