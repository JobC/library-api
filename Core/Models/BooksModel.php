<?php

class BooksModel extends Model
{
    public $tables = ['books'];
    private $table = 'books';
    private $fields = [
        [
            'label' => 'name',
            'required' => true
        ],
        [
            'label' => 'author',
            'required' => true
        ],
        [
            'label' => 'categories_id',
            'required' => true
        ],
        [
            'label' => 'published_at',
            'required' => true
        ],
        [
            'label' => 'users_id',
            'required' => false
        ]
    ];

    public function search($id = null){
        if(is_numeric($id)){
            if($book = $this->getOneRow(['from' => $this->table, 'where' => "id = $id"])){
                $this->set_200([
                    'data' => $book
                ]);
            }else{
                $this->set_404(['description' => 'Book not found']);
            }
        }else{
            $books = $this->getAllRows(['from' => $this->table]);
            $this->set_200([
                'data' => $books
            ]);
        }
    }

    public function store($request, $id = null)
    {
        $data = $this->checkModel($request);
        if(is_null($id)){
            $this->begin();
            if($id = $this->insert($this->table, $data)){
                if($book = $this->getOneRow(['from' => $this->table, 'where' => "id = $id"])){
                    $this->commit();
                    $this->set_201([
                        'data' => $book
                    ]);
                }else{
                    $this->rollback();
                    $this->set_400();
                }
            }else{
                $this->rollback();
                $this->set_400();
            }
        }else{
            $this->begin();
            if($this->update($this->table, $data, "id = $id")){
                if($book = $this->getOneRow(['from' => $this->table, 'where' => "id = $id"])){
                    $this->commit();
                    $this->set_200([
                        'data' => $book
                    ]);
                }else{
                    $this->rollback();
                    $this->set_400();
                }
            }else{
                $this->rollback();
                $this->set_400();
            }
        }
    }

    public function destroy($id = null){
        if(is_null($id)){
            $this->set_400(['description' => 'id is required']);
        }

        if($book = $this->getOneRow(['from' => $this->table, 'where' => "id = $id"])){

            if($this->delete($this->table, "id = $id")){
                $this->set_200(['description' => 'Deleted successfully']);
            }else{
                $this->set_400();
            }
        }else{
            $this->set_400(['description' => 'book not found']);
        }


    }

    private function checkModel($request = [])
    {
        $data = [];
        if (empty($request)) {
            $this->set_400(['description' => "Can't send a void requests"]);
        }

        foreach ($this->fields as $k => $field) {
            if (isset($request[$field['label']])) {
                $data[$field['label']] = $request[$field['label']];

                if ($field['required'] AND empty($request[$field['label']]) ) {
                    $this->set_400(['description' => "$field is required"]);
                }
            }
        }
        return $data;
    }
}
